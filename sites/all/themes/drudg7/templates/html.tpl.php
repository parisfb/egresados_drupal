<?php print $doctype; ?>
<html lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>"<?php print $rdf->version . $rdf->namespaces; ?>>
<head<?php print $rdf->profile; ?>>
  <!-- Carga de la barra de accesos de la UdeG -->
<link rel="shortcut icon" href="/sites/default/files/favicon.ico" type="image/vnd.microsoft.icon">
<script type="text/javascript" src="http://www.udg.mx/menu/udg-menu.js"></script>
<script type="text/javascript">
	var UDGMenuWidth ='100%';
	document.writeln( crearTag( 'div', 'id="udg_menu_principal"' ) ); setTimeout( 'UDGMenu()', 20 );
</script>
<link rel='stylesheet' type='text/css' href='http://www.udg.mx/menu/udg-menu.css'>
<!-- Fin de la carga de la barra de accesos de la UdeG -->
  <?php print $head; ?>
  <title><?php print $head_title; ?></title>  
  <?php print $styles; ?>
  <?php print $scripts; ?>
  <!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
</head>
<body<?php print $attributes;?>>
  <div id="skip-link">
    <a href="#main-content" class="element-invisible element-focusable"><?php print t('Skip to main content'); ?></a>
  </div>
  <?php print $page_top; ?>
  <?php print $page; ?>
  <?php print $page_bottom; ?>
<div id="fb-root"></div> 
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v8.0&appId=337363654000891&autoLogAppEvents=1" nonce="SMkVw1P1"></script>
</body>
</html>
